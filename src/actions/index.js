import { IMAGES, STATS } from "../constants";

export const loadImages = () => {
  return {
    type: IMAGES.LOAD,
  };
};

export const setImages = images => {
  return {
    type: IMAGES.LOAD_SUCCESS,
    payload: images,
  };
};

export const setError = (error) => {
  return {
    type: IMAGES.LOAD_FAIL,
    payload: error,
  };
};

export const loadImagesStats = (id) => {
  return {
    type: STATS.LOAD,
    payload: id,
  };
};

export const setImagesStats = (id, downloads) => {
  return {
    type: STATS.LOAD_SUCCESS,
    payload: { id, downloads },
  };
};

export const setErrorStats = (id) => {
  return {
    type: STATS.LOAD_FAIL,
    payload: id,
  };
};
