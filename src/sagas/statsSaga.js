import { call, fork, put, take } from 'redux-saga/effects';
import { loadImagesStats, setErrorStats, setImagesStats } from '../actions';
import { fetchImageStats } from '../api'
import { IMAGES } from '../constants';

function* handleStatsRequest(id) {
    for (let i = 0; i < 3; i++) {
        try {
            yield put(loadImagesStats(id));
            const res = yield call(fetchImageStats, id);
            yield put(setImagesStats(id, res.downloads.total));
            return
        } catch (error) {

        }
    }

    yield put(setErrorStats(id))
}

export default function* watchStatsRequest() {
    while (true) {
        const { payload } = yield take(IMAGES.LOAD_SUCCESS);

        for (let i = 0; i < payload.length; i++) {
            yield fork(handleStatsRequest, payload[i].id);
        }
    }
}
