import { takeEvery, select, call, put } from 'redux-saga/effects'

import { IMAGES } from '../constants'
import {fetchImages} from '../api';
import { setError, setImages } from '../actions';

const getPage = state => state.nextPage;

function* handleImagesLoad() {
    try {
        const page = yield select(getPage);
        const images = yield call(fetchImages, page); 
        // console.log("call");
        yield put(setImages(images));
    } catch (error) {
        yield put(setError(error.toString()));
    }
}   

export default function* watchImagesLoad() {
    yield takeEvery(IMAGES.LOAD, handleImagesLoad);
}
    